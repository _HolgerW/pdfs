import unittest
from pathlib import Path
from pdfsearch import pdfs as ps


class Testpdfs(unittest.TestCase):
    def test_true_contains_keyword(self):
        p1 = Path.cwd()/'tests'/'Test1pdf.pdf'
        self.assertEqual([p1.as_uri()], list(ps.contains_keyword([Path.cwd() /
                         'tests'], ["Physics"], False, 5, False, False)))
        self.assertEqual([p1.as_uri()], list(ps.contains_keyword([Path.cwd() /
                         'tests'], ["Test"], False, False, True, False)))
        self.assertEqual([], list(ps.contains_keyword([Path.cwd()/'tests'],
                         ["Absurdity"], False, 2, False, False)))

    def test_raises_Main_SystemExit(self):
        with self.assertRaises(SystemExit):
            ps.Main(['pdfs.py'])
        with self.assertRaises(SystemExit):
            ps.Main(['pdfs.py', '-n', '2'])
        with self.assertRaises(SystemExit):
            ps.Main(['pdfs.py', '-k'])

    def test_Main(self):
        self.assertEqual(0, ps.Main(['pdfs.py', '-f', 'Python']))
        self.assertEqual(0, ps.Main(['pdfs.py', '-n 2', 'Physics']))
