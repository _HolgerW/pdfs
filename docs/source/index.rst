.. pdfs - pdf search Tool documentation master file, created by
   sphinx-quickstart on Thu Jul 30 21:47:49 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pdfs - pdf search Tool's documentation!
==================================================

.. automodule:: src.pdfs
   :members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
